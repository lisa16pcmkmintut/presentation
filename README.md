# My project's README

This repo holds the PDFs of the documents I've updated since I sent them to Usenix for inclusion on the conference USB stick in October 2016.

There are quite a few changes.

If you just want to follow along with the slide deck, here's a URL to the slide published on Google Slide Slinger:

   [The URL Shortener Didn't Work So Here's Some Link Text](https://docs.google.com/presentation/d/1befxm0UOAfgmjU6Gdhcaf7qSp68o_Ecrp_loemMqUxw/pub?start=false&loop=false&delayms=60000)